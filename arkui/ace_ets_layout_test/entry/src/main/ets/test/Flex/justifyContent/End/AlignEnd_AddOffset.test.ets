/*
 * Copyright (c) 2023-2030 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "@ohos/hypium";
import router from '@system.router';
import CommonFunc from "./../../../../MainAbility/common/Common"
import {MessageManager,Callback} from './../../../../MainAbility/common/MessageManager';
export default function AlignEnd_AddOffset() {
  describe('AlignEnd_AddOffset', function () {
    beforeEach(async function (done) {
      let options = {
        uri: 'MainAbility/pages/Flex/justifyContent/End/AlignEnd_AddOffset',
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get AlignEnd_AddOffset state success " + JSON.stringify(pages));
        if (!("AlignEnd_AddOffset" == pages.name)) {
          console.info("get AlignEnd_AddOffset state pages.name " + JSON.stringify(pages.name));
          let result = await router.push(options);
          await CommonFunc.sleep(2000);
          console.info("push AlignEnd_AddOffset page success " + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push AlignEnd_AddOffset page error " + JSON.stringify(err));
        expect().assertFail();
      }
      done()
    });

    afterEach(async function () {
      await CommonFunc.sleep(2000);
      console.info("AlignEnd_AddOffset after each called");
    });

    /**
     * @tc.number    SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_END_TEST_0900
     * @tc.name      Align_End_Row_AddOffset
     * @tc.desc      The interface display of the component that sets the offset position when drawing
     */
    it('SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_END_TEST_0900', 0, async function (done) {
      console.info('[SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_END_TEST_0900] START');
      globalThis.value.message.notify({name:'OneOffset', value:{ x: 10, y: 15 }})
      await CommonFunc.sleep(3000);
      let strJson = getInspectorByKey('End_AddAlign_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Flex');
      expect(obj.$attrs.constructor.direction).assertEqual('FlexDirection.Row');
      expect(obj.$attrs.constructor.justifyContent).assertEqual('FlexAlign.End');
      let End_AddOffset_011 = CommonFunc.getComponentRect('End_AddOffset_011');
      let End_AddOffset_012 = CommonFunc.getComponentRect('End_AddOffset_012');
      let End_AddOffset_013 = CommonFunc.getComponentRect('End_AddOffset_013');
      let End_AddOffset_01 = CommonFunc.getComponentRect('End_AddOffset_01');
      expect(End_AddOffset_011.top - End_AddOffset_01.top).assertEqual(vp2px(15));
      expect(End_AddOffset_011.right - End_AddOffset_012.left).assertEqual(vp2px(10));
      expect(End_AddOffset_011.left - End_AddOffset_01.left).assertEqual(vp2px(60));
      expect(End_AddOffset_012.top).assertEqual(End_AddOffset_013.top);
      expect(End_AddOffset_012.top).assertEqual(End_AddOffset_01.top);
      expect(End_AddOffset_013.left).assertEqual(End_AddOffset_012.right);
      expect(End_AddOffset_01.right).assertEqual(End_AddOffset_013.right);
      expect(End_AddOffset_011.right - End_AddOffset_011.left).assertEqual(vp2px(150));
      expect(End_AddOffset_012.right - End_AddOffset_012.left).assertEqual(vp2px(150));
      expect(End_AddOffset_013.right - End_AddOffset_013.left).assertEqual(vp2px(150));
      expect(End_AddOffset_011.bottom - End_AddOffset_011.top).assertEqual(vp2px(50));
      expect(End_AddOffset_012.bottom - End_AddOffset_012.top).assertEqual(vp2px(100));
      expect(End_AddOffset_013.bottom - End_AddOffset_013.top).assertEqual(vp2px(150));
      console.info('[SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_END_TEST_0900] END');
      done();
    });
  })
}
